# VHDL GLdL Utilities

Utilities to use when developing with VHDL

## Projects

## Files:
- vhdl_tb_utils_pkg
  - procedures to print to console
- vhdl_textio_csv_pkg
  - csv "class" : csv_file_type to read and write CSV files
