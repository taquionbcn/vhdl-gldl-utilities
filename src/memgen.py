'''
Filename: /home/guille/ATLAS/l0mdt-hdl-design/shared/vhdl_utilities/src/memgen.py
Path: /home/guille/ATLAS/l0mdt-hdl-design/shared/vhdl_utilities/src
Created Date: Saturday, January 14th 2023, 11:02:41 pm
Author: Guillermo Loustau de Linares

Copyright (c) 2023 UMass , Physics Department
'''




import argparse
import sys


def gen_math_table(args):
  """
  Purpose: arg
  """
  addr_bits=8
  out_scale=10
  
  max_addr=2**addr_bits
  
  print(f"addr_bits : {addr_bits}")
  print(f"out_scale : {out_scale}")
  print(f"max_addr : {max_addr}")
  
# end def

def main(args):
  """
  Purpose: 
  """
  print(f"{type(args)}")
  print(f"{(args)}")
  for key,val in vars(args).items():
    print(f"{key} : {val}")
  
  
  if args.type == "tan":
    gen_math_table(args)


if __name__ == '__main__':
  
  epilog = "Examples of use:\n"
  switches = {}
  
  parser = argparse.ArgumentParser(
      prog=None, 
      usage=None, 
      description=None, 
      epilog=None, 
      parents=[], 
      formatter_class=argparse.HelpFormatter, 
      prefix_chars='-', 
      fromfile_prefix_chars=None, 
      argument_default=None, 
      conflict_handler='error', 
      add_help=True, 
      allow_abbrev=True
    )
  
  for switch in sorted(switches, key=lambda s: s[0].lower()):
    parser.add_argument(*switch, **switches[switch])

  h_src = "Sorce memory file path"
  parser.add_argument('src',
                      help=("Sorce memory file path."),
                      default=None,
                      nargs='?')
  parser.add_argument("type",
                      help=("Type of memory to generate."),
                      default=None,
                      nargs='?')
  parser.add_argument("-s",
                      "--scale",
                      help=("scalate ."),
                      default=0,
                      type=int,
                      nargs='?')
  
  if len(sys.argv)==1:
    parser.print_usage()
    sys.exit(0)

  args = parser.parse_args()
  
  main(args)
  
  