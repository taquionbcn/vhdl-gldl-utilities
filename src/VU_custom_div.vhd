--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: src
-- File: VU_custom_div.vhd
-- Module: <<moduleName>>
-- File PATH: /VU_custom_div.vhd
-- -----
-- File Created: Tuesday, 6th December 2022 12:36:42 am
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Tuesday, 6th December 2022 12:38:49 am
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
-- use ieee.std_logic_misc.all;
use std.textio.all;
-- use std.standard.all;

library shared_lib;
-- use shared_lib.VU_custom_div_pkg.all;

entity VU_custom_div is
  generic(
    g_NUMERATOR_LEN     : integer   := 8;
    g_DENOMINATOR_LEN   : integer   := 8;
    g_QUOTIENT_LEN      : integer   := 8;
    g_MEM_WIDTH         : integer   := 16;
    g_SCALAR            : unsigned  := "0";
    g_SCALAR_10X        : unsigned  := "0";
    g_MEMORY_FILE       : string    := "not_defined.mem";
    g_ROM_STYLE         : string    := "auto";
    g_MEMORY_TYPE       : string    := "auto";
    g_IN_PIPE_STAGES    : integer := 2;
    g_OUT_PIPE_STAGES   : integer := 2
  );
  port (
    clk       : in std_logic;
    rst       : in std_logic;
    ena       : in std_logic;
    --
    i_num     : in std_logic_vector(g_NUMERATOR_LEN - 1 downto 0);
    i_num_dv  : in std_logic;
    i_den     : in std_logic_vector(g_DENOMINATOR_LEN - 1 downto 0);
    i_den_dv      : in std_logic;
    o_res     : out std_logic_vector(g_QUOTIENT_LEN - 1 downto 0);
    o_dv      : out std_logic
  );
end VU_custom_div ;

architecture arch of VU_custom_div is
  --
    -- function f_dig_get_mem_depth(g_SCALAR,g_SCALAR_10X : integer; g_QUOTIENT_LEN : integer) return integer is
    --   variable o_md : integer;
    -- begin
    --   o_md := 2**g_QUOTIENT_LEN;
    --   return o_md;
    -- end function;

    -- type div_mem_t is array (0 to g_MEM_WIDTH - 1) of std_logic_vector(g_QUOTIENT_LEN - 1 downto 0);

    -- function f_div_init_mem(g_SCALAR , g_SCALAR_10X : unsigned ; g_QUOTIENT_LEN : integer) return div_mem_t is
    --   variable o_mem : div_mem_t;
    --   constant scalar : unsigned(g_QUOTIENT_LEN - 1 downto 0) := resize(g_SCALAR * g_SCALAR_10X,g_QUOTIENT_LEN);
    -- begin
    --   -- scalar := g_SCALAR * g_SCALAR_10X;
    --   -- scalar10x := g_SCALAR_10X;
    --   assert false report 
    --     "VU_custom_div => " & 
    --     " g_MEM_WIDTH " & integer'image(g_MEM_WIDTH) & 
    --     " g_SCALAR : " & integer'image(to_integer(g_SCALAR)) & 
    --     " g_QUOTIENT_LEN : " & integer'image(g_QUOTIENT_LEN)
    --   severity note;
    --   -- assert false report  "Initialization of memory" severity note;
    --   for i in 0 to g_MEM_WIDTH - 1 loop
    --     if i = 0 then
    --       o_mem(i) := std_logic_vector(resize(scalar,g_QUOTIENT_LEN));
    --     else
    --       o_mem(i) := std_logic_vector(resize(scalar/to_unsigned(i,g_QUOTIENT_LEN),g_QUOTIENT_LEN));
    --     end if;
    --   end loop;
    --   assert false report  "Done initialization of memory" severity note;
    --   return o_mem;
    -- end function;

    -- signal mem : div_mem_t := f_div_init_mem(g_SCALAR,g_SCALAR_10X,g_QUOTIENT_LEN);
    -- attribute rom_style        : string;
    -- attribute rom_style of mem : signal is g_MEMORY_TYPE;

    -- signal pl0_res : std_logic_vector(g_QUOTIENT_LEN - 1 downto 0);
    -- signal pl0_dv  : std_logic;
    -- signal pl0_ena  : std_logic;
  --
  
  signal i_num_pl     : std_logic_vector(g_NUMERATOR_LEN - 1 downto 0);
  signal bden_inv_res : std_logic_vector(50 -1 downto 0);
  signal bden_inv_dv  : std_logic;

  signal bdiv_vu_res_descale  : std_logic_vector(39 -1 downto 0);
  
  signal bdiv_vu_res          : std_logic_vector(83 -1 downto 0);
  signal bdiv_vu_dv           : std_logic;

  COMPONENT VU_rom
  GENERIC (
    MXADRB : INTEGER;
    MXDATB : INTEGER;
    ROM_FILE : STRING;
    ROM_STYLE : STRING
  );
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR;
    dvin : IN STD_LOGIC;
    douta : OUT STD_LOGIC_VECTOR;
    dvout : out STD_LOGIC

  );
  END COMPONENT;

begin

  -- mem_rd: process(clk)
    -- begin
    --   if rising_edge(clk) then
    --     if rst = '1' then
    --       o_dv <= '0';
    --       pl0_dv <= '0';
    --       pl0_res <= (others => '0');
    --       o_res <= (others => '0');
    --     else
    --       if ena = '1' then
    --         if i_dv = '1' then
    --           pl0_res <= mem(to_integer(unsigned(i_den)));
    --         end if;
    --         pl0_dv <= i_dv;
    --         pl0_ena <= ena;
    --       end if ;
    --       if pl0_ena = '1' then
    --         if pl0_dv = '1' then
    --           o_res <= pl0_res;
    --         end if;
    --         o_dv <= pl0_dv;
    --       end if ;      
    --     end if;
    --   end if;
  -- end process;

 ctrl: process(clk)
 begin
  if rising_edge(clk) then
    if rst = '1' then
      i_num_pl  <= (others => '0');
    else
      i_num_pl  <= i_num;
    end if;
  end if;
 end process ctrl;

  VU_ROM_INST : VU_rom
  GENERIC MAP(
    MXADRB    => i_den'length - 1,
    MXDATB    => bden_inv_res'length,
    ROM_FILE  => g_MEMORY_FILE,
    ROM_STYLE => g_MEMORY_TYPE
  )
  PORT MAP(
    ena   => '1',
    clka  => clk,
    addra => i_den,
    dvin  => i_den_dv,
    douta => bden_inv_res,
    dvout => bden_inv_dv
  );

  DIV_b_VUX : entity shared_lib.VU_generic_pipelined_MATH
  generic map(
    g_OPERATION => "*",
    g_IN_PIPE_STAGES  => 5,
    g_OUT_PIPE_STAGES => 5,
    g_in_A_WIDTH => i_num_pl'length,
    g_in_B_WIDTH => bden_inv_res'length
  )
  port map(
    clk         => clk,
    rst         => rst,
    --
    i_in_A      => i_num_pl,
    i_in_B      => bden_inv_res,
    -- i_in_C      => "0",
    -- i_in_D      => "0",
    i_dv        => bden_inv_dv,
    --
    o_result    => bdiv_vu_res,
    o_dv        => bdiv_vu_dv 
);
  bdiv_vu_res_descale <= bdiv_vu_res(60 -1 downto 21);



end architecture ; -- arch