--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: src
-- File: VU_generic_pipelined_MATH.vhd
-- Module: <<moduleName>>
-- File PATH: /VU_generic_pipelined_MATH.vhd
-- -----
-- File Created: Monday, 9th January 2023 9:13:57 am
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Monday, 9th January 2023 9:14:16 am
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
-- 2023-01-13	GLdL	Added ctrl of input port widths
-- 2023-01-17	GLdL	Added ctrl of output port width
--------------------------------------------------------------------------------
--ORIGINAL: https://forums.xilinx.com/t5/Synthesis/DSP48E2-not-pipelined-DRC-DPOP-4-warning/m-p/1209090#M37753


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library shared_lib;
use shared_lib.VU_generic_pipelined_MATH_pkg.all;


entity VU_generic_pipelined_MATH is
  generic (
    g_INFER_DSP       : std_logic := '0';
    g_NAME            : string := "Empty";
    g_OPERATION       : string;--:= "*";
    g_IN_PIPE_STAGES  : integer := 2;      -- specifies how many pipeline registers to instantiate at the input of multiplier

    g_OUT_PIPE_STAGES : integer := 2;       -- number of the pipeline registers to instantiate at the output of multiplier

    g_IN_DEFAULT_VAL : std_logic_vector(0 downto 0) := (others => '0');
    g_in_A_WIDTH : integer := 0;
    g_in_B_WIDTH : integer := 0;
    g_in_C_WIDTH : integer := 0;
    g_in_D_WIDTH : integer := 0;
    g_in_A_TYPE : string := "signed";
    g_in_B_TYPE : string := "signed";
    g_in_C_TYPE : string := "signed";
    g_in_D_TYPE : string := "signed";
    g_OUT_WIDTH : integer := arith_get_out_width(g_OPERATION,0,g_in_A_WIDTH,g_in_B_WIDTH,g_in_C_WIDTH,g_in_D_WIDTH);
    g_MAIN_MATH_MODE : string := "normal"
    );
  port (
    -- clock and reset signals
    clk           : in std_logic;
    rst           : in std_logic;
    -- input operands
    i_in_A        : in std_logic_vector(g_in_A_WIDTH -1 downto 0) := (others => '0');--:= g_IN_DEFAULT_VAL;--(OPERAND_A_WIDTH-1 downto 0);
    i_in_B        : in std_logic_vector(g_in_B_WIDTH -1 downto 0) := (others => '0');--:= g_IN_DEFAULT_VAL;--(OPERAND_B_WIDTH-1 downto 0);
    i_in_C        : in std_logic_vector(g_in_C_WIDTH -1 downto 0) := (others => '0');--:= g_IN_DEFAULT_VAL;--(OPERAND_C_WIDTH-1 downto 0);
    i_in_D        : in std_logic_vector(g_in_D_WIDTH -1 downto 0) := (others => '0');--:= g_IN_DEFAULT_VAL;--(OPERAND_C_WIDTH-1 downto 0);
    i_dv          : in std_logic; -- specifies that A and B inputs are valid, so their values can be propagated through the pipeline
    -- output product
    o_result      : out std_logic_vector(g_OUT_WIDTH -1 downto 0) ;-- := (others => '0');--((g_RESULT_WIDTH-1) downto 0);
    o_dv          : out std_logic -- valid signal for an output product
  );
end VU_generic_pipelined_MATH;

architecture beh of VU_generic_pipelined_MATH is

  constant OPERAND_A_WIDTH : integer := i_in_A'length;--arith_get_in_width(i_in_A'length,to_integer(unsigned(i_in_A)));
  constant OPERAND_B_WIDTH : integer := i_in_B'length;--arith_get_in_width(i_in_B'length,to_integer(unsigned(i_in_B)));
  constant OPERAND_C_WIDTH : integer := i_in_C'length;--arith_get_in_width(i_in_C'length,to_integer(unsigned(i_in_C)));
  constant OPERAND_D_WIDTH : integer := i_in_D'length;--arith_get_in_width(i_in_D'length,to_integer(unsigned(i_in_D)));
  constant OPERAND_O_WIDTH : integer := arith_get_out_width(g_OPERATION,0,OPERAND_A_WIDTH,OPERAND_B_WIDTH,OPERAND_C_WIDTH,OPERAND_D_WIDTH);
  constant g_RESULT_WIDTH  : integer := arith_get_out_width(g_OPERATION,g_OUT_WIDTH,OPERAND_A_WIDTH,OPERAND_B_WIDTH,OPERAND_C_WIDTH,OPERAND_D_WIDTH);

  constant TOTAL_MUL_LATENCY : integer := g_IN_PIPE_STAGES + g_OUT_PIPE_STAGES;

  type data_pl_A_t is array (integer range <>) of std_logic_vector(OPERAND_A_WIDTH-1 downto 0);
  type data_pl_B_t is array (integer range <>) of std_logic_vector(OPERAND_B_WIDTH-1 downto 0);
  type data_pl_C_t is array (integer range <>) of std_logic_vector(OPERAND_C_WIDTH-1 downto 0);
  type data_pl_D_t is array (integer range <>) of std_logic_vector(OPERAND_D_WIDTH-1 downto 0);

  type data_pl_O_t is array (integer range <>) of std_logic_vector((g_RESULT_WIDTH-1) downto 0);

  signal mul_in_pipe_A : data_pl_A_t(g_IN_PIPE_STAGES -1 downto 0) ;-- := (others => (others => '0'));
  signal mul_in_pipe_B : data_pl_B_t(g_IN_PIPE_STAGES -1 downto 0) ;-- := (others => (others => '0'));
  signal mul_in_pipe_C : data_pl_C_t(g_IN_PIPE_STAGES -1 downto 0) ;-- := (others => (others => '0'));
  signal mul_in_pipe_D : data_pl_D_t(g_IN_PIPE_STAGES -1 downto 0) ;-- := (others => (others => '0'));

  signal pl_transfer_A : std_logic_vector(OPERAND_A_WIDTH-1 downto 0) ;-- := (others => '0');
  signal pl_transfer_B : std_logic_vector(OPERAND_B_WIDTH-1 downto 0) ;-- := (others => '0');
  signal pl_transfer_C : std_logic_vector(OPERAND_C_WIDTH-1 downto 0) ;-- := (others => '0');
  signal pl_transfer_D : std_logic_vector(OPERAND_D_WIDTH-1 downto 0) ;-- := (others => '0');

  signal valid_signal_pipe : std_logic_vector(TOTAL_MUL_LATENCY downto 0)  ;-- := (others => '0');
  signal dv_pl_transfer : std_logic ;-- := '0';

  signal int_Result_sc : std_logic_vector(g_RESULT_WIDTH - 1 downto 0)  ;-- := (others => '0');

begin

  DV_PL_GEN: if g_IN_PIPE_STAGES = 0 generate
    dv_pl_transfer <= i_dv;
  else generate
    dv_pl_transfer <= valid_signal_pipe( g_IN_PIPE_STAGES-1);
  end generate;
  DV_PL : process(clk) begin
    if rising_edge(clk) then
      if rst = '1' then
        valid_signal_pipe <= (others => '0');
        -- o_dv <= '0';
      else

        if i_dv then
          valid_signal_pipe(0) <= i_dv;
        else
          valid_signal_pipe(0) <= '0';
        end if;

        for n in 1 to (TOTAL_MUL_LATENCY) loop
          valid_signal_pipe(n) <=  valid_signal_pipe(n-1);
        end loop;
        
      end if;
    end if;
  end process;

  IN_PL_GEN: if g_IN_PIPE_STAGES = 0 generate
    pl_transfer_A <= i_in_A;
    pl_transfer_B <= i_in_B;
  else generate
    pl_transfer_A <= mul_in_pipe_A(g_IN_PIPE_STAGES-1);
    pl_transfer_B <= mul_in_pipe_B(g_IN_PIPE_STAGES-1);
    IN_PL : process(clk) begin
      if rising_edge(clk) then
        if rst = '1' then
          mul_in_pipe_A <= (others => (others => '0'));
          mul_in_pipe_B <= (others => (others => '0'));

        else
          if i_dv then
            mul_in_pipe_A(0) <= i_in_A;
            mul_in_pipe_B(0) <= i_in_B;
          else
            mul_in_pipe_A(0) <= (others => '0');
            mul_in_pipe_B(0) <= (others => '0');
          end if;
  
          for i in 1 to (g_IN_PIPE_STAGES-1) loop
            mul_in_pipe_A(i) <=  mul_in_pipe_A(i-1);
            mul_in_pipe_B(i) <=  mul_in_pipe_B(i-1);
          end loop;
        end if;
      end if;
    end process;
  end generate IN_PL_GEN;

  C_PL: if OPERAND_C_WIDTH > 1 generate
      IN_PL_GEN: if g_IN_PIPE_STAGES = 0 generate
        pl_transfer_C <= i_in_C;
      else generate
      pl_transfer_C <= mul_in_pipe_C(g_IN_PIPE_STAGES-1);
        IN_PL : process(clk) begin
          if rising_edge(clk) then
            if rst = '1' then
              mul_in_pipe_C <= (others => (others => '0'));
            else
              if i_dv then
                mul_in_pipe_C(0) <= i_in_C;
              else
                mul_in_pipe_C(0) <= (others => '0');
              end if;
              for i in 1 to (g_IN_PIPE_STAGES-1) loop
                mul_in_pipe_C(i) <=  mul_in_pipe_C(i-1);
              end loop;
            end if;
          end if;
        end process;
      end generate IN_PL_GEN;
    end generate C_PL;
    
  D_PL: if OPERAND_D_WIDTH > 1 generate
      IN_PL_GEN: if g_IN_PIPE_STAGES = 0 generate
        -- mul_in_pipe_D(0) <= i_in_D;
        pl_transfer_D <= i_in_D;

      else generate
        pl_transfer_D <= mul_in_pipe_D(g_IN_PIPE_STAGES-1);
        IN_PL : process(clk) begin
          if rising_edge(clk) then
            if rst = '1' then
              mul_in_pipe_D <= (others => (others => '0'));
            else
              if i_dv then
                mul_in_pipe_D(0) <= i_in_D;
              else
                mul_in_pipe_D(0) <= (others => '0');
              end if;
              for i in 1 to (g_IN_PIPE_STAGES-1) loop
                mul_in_pipe_D(i) <=  mul_in_pipe_D(i-1);
              end loop;
            end if;
          end if;
        end process;
      end generate IN_PL_GEN;
    end generate D_PL;

  MATH_MODE: if g_MAIN_MATH_MODE = "normal" generate
    signal int_Result_s : signed(OPERAND_O_WIDTH - 1 downto 0) ;-- := (others => '0');
    -- attribute use_dsp             : string;
    -- attribute use_dsp of int_Result_s : signal is "yes";
  begin

    MATH : process(clk) begin
      if rising_edge(clk) then
        if rst = '1' then
          int_Result_s <= (others => '0');
        else
          if dv_pl_transfer = '1' then
            if g_OPERATION = "*" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*1" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*2" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*3" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*4" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*5" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "*6" then
              int_Result_s <=signed(pl_transfer_A) * signed( pl_transfer_B);
            elsif g_OPERATION = "-" then
                int_Result_s <=signed(pl_transfer_A) - signed( pl_transfer_B);
            elsif g_OPERATION = "--" then
              int_Result_s <=signed(pl_transfer_A) - signed( pl_transfer_B);
            elsif g_OPERATION = "*-" then
                int_Result_s <=(signed(pl_transfer_A) * signed( pl_transfer_B)) - signed( pl_transfer_C);
            elsif g_OPERATION = "/" then
                int_Result_s <=signed(pl_transfer_A) / signed( pl_transfer_B);
            elsif g_OPERATION = "+" then
              int_Result_s <=resize(signed( pl_transfer_A),g_RESULT_WIDTH) + 
                resize(signed( pl_transfer_B),g_RESULT_WIDTH);
            elsif g_OPERATION = "+++" then
                int_Result_s <=resize(signed( pl_transfer_A),g_RESULT_WIDTH) + 
                resize(signed( pl_transfer_B),g_RESULT_WIDTH) +
                resize(signed( pl_transfer_C),g_RESULT_WIDTH) +
                resize(signed( pl_transfer_D),g_RESULT_WIDTH);
            end if;
          else
            int_Result_s <= (others => '0');
          end if;
        end if;
      end if;
    end process;

    int_Result_sc <= std_logic_vector(resize(int_Result_s,g_RESULT_WIDTH));

  elsif g_MAIN_MATH_MODE = "div4" generate
    constant div_scaler : integer := 4;
    constant num_sums_group : integer := 2;
    constant num_ele : integer := integer(ceil((real(g_in_A_WIDTH)/2.0)))-1;
    constant for_lim : integer := (g_in_A_WIDTH + div_scaler)/(num_sums_group*2);
    
    signal div_nom : unsigned(g_in_A_WIDTH - 1 downto 0);
    signal div_nom_sc : std_logic_vector(g_in_A_WIDTH + div_scaler -1 downto 0);
    signal div_den : unsigned(g_in_B_WIDTH - 1 downto 0);
    -- signal div_den_pl : unsigned(g_in_B_WIDTH - 1 downto 0);
    signal div_out : std_logic_vector(g_in_A_WIDTH + div_scaler -1 downto 0);
    signal div_out_2 : std_logic_vector(g_in_A_WIDTH + div_scaler -1 downto 0);
    signal div_out_4 : std_logic_vector(g_in_A_WIDTH + div_scaler -1 downto 0);
    -- signal div_out_pl : unsigned(OPERAND_O_WIDTH-1 downto 0);
    signal int_Result_u : unsigned(g_in_A_WIDTH + div_scaler - 1 downto 0) ;-- := (others => '0');

    signal zeros : std_logic_vector(g_in_A_WIDTH + div_scaler - 1 downto 0) ;-- := (others => '0');

    type aux_sums_t is array (num_ele-1 downto 0) of std_logic_vector(g_in_A_WIDTH + div_scaler - 1 downto 0);
    signal aux_sums : aux_sums_t;

    -- signal div4_dv_pl : std_logic;
    signal aux_sum : unsigned(g_in_A_WIDTH + div_scaler - 1 downto 0);
    attribute use_dsp             : string;
    attribute use_dsp of int_Result_u : signal is "yes";


    begin

    div_nom <= unsigned(pl_transfer_A);
    div_den <= unsigned(pl_transfer_B);

    div_out_2 <= "0" & div_nom_sc(div_nom_sc'length -1 downto 1);
    div_out_4 <= "00" & div_nom_sc(div_nom_sc'length -1 downto 2);

    div_nom_sc <=  pl_transfer_A & "0000" ;
    sums_gen : for i in 0 to num_ele - 1 generate
      aux_sums(i) <= zeros(i*2 + 1 downto 0) & div_nom_sc(g_in_A_WIDTH + div_scaler - 1 downto 2 + 2*i);
    end generate ;

    MATH : process(clk) 
      -- variable v_aux_nom : std_logic_vector(g_in_A_WIDTH - 1 downto 0);
      variable v_aux_sum : unsigned(g_in_A_WIDTH + div_scaler - 1 downto 0);
    begin
      if rising_edge(clk) then
        if rst = '1' then
          int_Result_u <= (others => '0');
          aux_sum <= (others => '0');
        else
          -- div4_dv_pl <= dv_pl_transfer;
          -- div_den_pl <= div_den;
          if dv_pl_transfer = '1' then
            if g_OPERATION = "/" then
              if div_den = 1 then
                -- div_out <= div_nom_sc;
                int_Result_u <= unsigned(div_nom_sc);

              elsif div_den = 2 then
                int_Result_u <= unsigned(div_out_2);
              elsif div_den = 3 then
                v_aux_sum := (others=>'0');
                for i in num_ele - 1 downto 0 loop
                  v_aux_sum := v_aux_sum + unsigned(aux_sums(i)) ;
                  -- v_aux_sum := v_aux_sum + unsigned(aux_sums(i)) ;
                end loop;
                  int_Result_u <= v_aux_sum;
              elsif div_den = 4 then
                int_Result_u <= unsigned(div_out_4);
                
              else
                int_Result_u <= (others => '1');
              end if;
            end if;
          else
            int_Result_u <= (others => '0');
          end if;

          -- if div4_dv_pl = '1' then
          --   if div_den_pl = 3 then 
          --     int_Result_u <= aux_sum;--resize(aux_sum(31 downto 4),OPERAND_O_WIDTH);
          --   else
          --     int_Result_u <= unsigned( div_out);--resize(div_out,g_RESULT_WIDTH);
          --   end if;
          -- else
          --   int_Result_u <= (others => '0');--std_logic_vector(resize(div_out,g_RESULT_WIDTH));
          -- end if;

        end if;
      end if;
    end process;

    -- int_Result_s <= std_logic_vector(div_out);
    -- int_Result_sc <= std_logic_vector(resize(div_out,g_RESULT_WIDTH));
    int_Result_sc <= std_logic_vector(resize(int_Result_u(OPERAND_O_WIDTH - 1 downto 4),g_RESULT_WIDTH));


  end generate;

  -- mul_output_pipe(0) <= int_Result_s;
  OUT_PL_GEN: if g_OUT_PIPE_STAGES = 0 generate
    o_result <= int_Result_sc;
  else  generate
    signal mul_output_pipe : data_pl_O_t(g_OUT_PIPE_STAGES -1 downto 0) ;-- := (others => (others => '0'));
  begin
    OUT_PL : process(clk) begin
      if rising_edge(clk) then
        if rst = '1' then
          mul_output_pipe <= (others => (others => '0'));
          -- o_result <= (others => '0');
        else
          mul_output_pipe(0) <= int_Result_sc;
          for j in 1 to (g_OUT_PIPE_STAGES-1) loop
            mul_output_pipe(j) <=  mul_output_pipe(j-1);
          end loop;
          -- o_result <= mul_output_pipe(g_OUT_PIPE_STAGES-1);
        end if;
      end if;
    end process;
    o_result <= mul_output_pipe(g_OUT_PIPE_STAGES-1);
  end generate OUT_PL_GEN;



  
  o_dv <= valid_signal_pipe(TOTAL_MUL_LATENCY);
  
end architecture beh;