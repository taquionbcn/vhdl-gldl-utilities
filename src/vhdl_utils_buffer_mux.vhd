--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: src
-- File: vhdl_utils_buffer_mux.vhd
-- Module: <<moduleName>>
-- File PATH: /vhdl_utils_buffer_mux.vhd
-- -----
-- File Created: Wednesday, 23rd November 2022 12:37:35 pm
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Wednesday, 23rd November 2022 12:37:42 pm
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------

-- library ieee;
-- use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;
-- use ieee.std_logic_misc.all;
-- use ieee.math_real.all;


-- entity vhdl_utils_buffer_mux is
--   generic(
--     g_NUM_INPUTS    : integer := 6,
--     g_DATA_WIDTH    : integer := 8
--    );
--   port (
--     clk           : in std_logic;
--     rst           : in std_logic;
--     glob_en       : in std_logic;
--     -- configuration
--     i_control     : in heg_ctrl2hp_art(g_HPS_NUM_MDT_CH -1 downto 0);
--     -- MDT in
--     i_data        : in heg_hp2bm_avt(g_HPS_NUM_MDT_CH-1 downto 0);
--     in_dv         : in std_logic_vector(g_NUM_INPUTS -1 downto 0);
--     -- MDT out
--     o_data        : out std_logic_vector(g_DATA_WIDTH - 1 downto 0);
--     o_dv          : out std_logic
    
--   );
-- end entity vhdl_utils_buffer_mux;

-- architecture beh of vhdl_utils_buffer_mux is

   

-- begin

-- end beh ;