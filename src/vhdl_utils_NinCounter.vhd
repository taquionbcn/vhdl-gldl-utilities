--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: src
-- File: vhdl_utils_NinCounter.vhd
-- Module: <<moduleName>>
-- File PATH: /vhdl_utils_NinCounter.vhd
-- -----
-- File Created: Wednesday, 23rd November 2022 2:34:49 pm
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Thursday, 24th November 2022 5:04:47 pm
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library shared_lib;

entity vhdl_utils_NinCounter is
  generic(
    g_NUM_INPUTS  : integer := 1;
    g_DATA_WIDTH  : integer
  );
  port (
    clk                 : in std_logic;
    rst                 : in std_logic;
    ena                 : in std_logic := '1';
    --
    i_triggers          : in std_logic_vector;
    o_counter           : out std_logic_vector(g_DATA_WIDTH  -1 downto 0)
  );
end entity vhdl_utils_NinCounter;

architecture beh of vhdl_utils_NinCounter is
  signal temp_counts :  unsigned(g_DATA_WIDTH  -1 downto 0) := (others => '0');
  signal total_counts :  unsigned(g_DATA_WIDTH  -1 downto 0) := (others => '0');

begin

  o_counter <= std_logic_vector(total_counts);
  
  counter: process(clk)
    variable v_temp_count :  unsigned(3 downto 0);
  begin
    if rising_edge(clk) then
      if rst = '1' then
        total_counts <= (others => '0');
        temp_counts <= (others => '0');
      else
        if ena = '1' then
          v_temp_count :=  (others => '0');
          for in_i in g_NUM_INPUTS -1 downto 0 loop
            -- if(i_triggers(in_i) = '1') then
            --   v_temp_count := v_temp_count + 1;
            -- end if;
            v_temp_count := v_temp_count +  resize(unsigned'( '0' & i_triggers(in_i)),4);


          end loop;
          temp_counts <= resize(v_temp_count,g_DATA_WIDTH);    
          total_counts <= total_counts + temp_counts;
        end if;
      end if;
    end if;
  end process counter;
  
  
  
end architecture beh;
