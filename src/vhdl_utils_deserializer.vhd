--------------------------------------------------------------------------------
--  UMass , Physics Department
--  Guillermo Loustau de Linares
--  guillermo.ldl@cern.ch
--------------------------------------------------------------------------------
--  Project: VHDL Tools
--  Module: vhdl_utils_deserializer 
--  Description:
--
--------------------------------------------------------------------------------
--  Revisions:
--      
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_misc.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity vhdl_utils_deserializer is
  generic(
    g_DATA_WIDTH        : integer
  );
  port (
    clk                 : in std_logic;
    rst                 : in std_logic;

    i_data              : in std_logic;
    o_data              : out std_logic_vector(g_DATA_WIDTH -1 downto 0)


  );
end entity vhdl_utils_deserializer;

architecture beh of vhdl_utils_deserializer is

  attribute keep_hierarchy : string;
  attribute keep_hierarchy of beh : architecture is "yes";

  signal data : std_logic_vector(g_DATA_WIDTH -1 downto 0);

  
begin

  des: process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        data <= (others => '0');
      else
        o_data <= data;
        for i_bit in g_DATA_WIDTH - 1 downto 1 loop
          data(i_bit) <= data(i_bit - 1);
        end loop;
        data(0) <= i_data;
      end if;
    end if;
  end process des;
  
  
  
end architecture beh;