#!/usr/bin/env bash
# -------------------------------
# guillermo.ldl@cern.ch
# L0MDT script to create start_sim.tcl for questa sim and link a waveform file
# -------------------------------

 
PROJ="$PWD"
TOOL="$PROJ/tools/gldl-scripts"

source "$TOOL/bash_tools.sh"

FULLPATH=""
MEM_WIDTH=""
MEM_DEPTH=""
MAX_VALUE=""
VALUE_DIV=""


function print_help () {
  echo " ====================================================="
  echo "                        HELP"
  echo " ====================================================="
  echo "  USAGE: setquestasim.sh [-h][-d] FILE_PATH MEM_WIDTH MEM_DEPTH MAX_VALUE VALUE_DIV"
  echo "       help : -h|--help"
  echo "              shows this message"
  echo "    verbose : -d|--debug"
  echo "              shows debug verbose"
  # echo "       path : -s|--searchpath"
  # echo "              selects path to search for waveform"
  # echo "    modules : -m|--module"
  # echo "              sets a custon name for the top tb"
  echo " ====================================================="
}


if [ -z "$1" ]; then
  echo_e "No arguments supplied"
  exit 1
else
  # for item in "$@"; do
  #   echo "${item}"
  # done
  # echo $#
  if [ $# -lt 5 ]; then
    echo "Not enought parameters"
    print_help
    exit 1
  # else
    # echo "no"
  fi

  while [[ $# -gt 0 ]]
  do
    if [ $# -lt 6 ]; then
      # echo "yes"
      break
    fi
    key="$1"
    # echo "key: $1"
    case "${key}" in
      -h|--help)
        print_help
        exit 0
      ;;
      -d|--debug)
      # if [ -z "$1" ]; then
        echo_w "DEBUG VERBOSE"
        DEBUG_VERBOSE=1
        shift 
      # else
        # DEBUG_VERBOSE="$2"
      # fi
      ;;
      # -s|--searchpath)
      # if [ -z "$2" ]; then
      #   echo_e "No arguments supplied"
      #   exit 1
      # else    
      #   CUSTOM_WAVE="$2"
      # fi
      # shift 
      # shift
      # ;;
      # -m|--module)
      # if [ -z "$2" ]; then
      #   echo_e "No arguments supplied"
      #   exit 1
      # else
      #   TOP_TB_NAME="$2"
      # fi
      # shift 
      # shift
      # ;;
      *)
      echo_w "Option $key no valid"
      shift
      # if [ -z "$key" ]; then
      #   echo_e "No arguments supplied"
      #   exit 1
      # else
      #   FULLNAME=$key
      #   shift
      # fi
    esac    
  done
fi

# seting file
FULLPATH=$1
FULLFILEPATH="$PROJ/$1"
ONLYPATH=${FULLFILEPATH%/*}
if [[ -d $ONLYPATH ]]; then
  echo_i "directory $ONLYPATH exists!!"
  if [[ -d "$PROJ/$FULLPATH" ]]; then
    echo_w "file $FULLFILEPATH exists and will be rewriten"
  else
    echo_w "file $FULLFILEPATH will be created!"
  fi
else
  echo_e "directory $ONLYPATH doesn't exists"
  exit 1
fi

# get ouput width
if ! [[ $2 =~ ^[xb]?[0-9]+$ ]] ; then
  echo_e "error: $2 Not a valid format"
  exit 1
else
  MEM_WIDTH=$2
  case "$2" in
    ^[0-9]+$)
      MEM_WIDTH=$2
    ;;
    x*)
      MEM_WIDTH=$((16#${MEM_WIDTH:1}))
    ;;
    b*)
      # MEM_WIDTH=$((16#${MEM_WIDTH:1}))
    ;;
  esac
  echo_i "output width : $MEM_WIDTH"
fi

# get mem depth
if ! [[ $3 =~ ^[xb]?[0-9]+$ ]] ; then
  echo_e "error: $3 Not a valid format"
  exit 1
else
  MEM_DEPTH=$3
  case "$MEM_DEPTH" in
    ^[0-9]+$)
      MEM_DEPTH=$2
    ;;
    x*)
      MEM_DEPTH=$((16#${MEM_DEPTH:1}))
    ;;
    b*)
      # MEM_DEPTH=$((16#${MEM_DEPTH:1}))
    ;;
  esac
  echo_i "memory depth : $MEM_DEPTH"
fi

# get max value 
if ! [[ $4 =~ ^[xb]?[0-9]+$ ]] ; then
  echo_e "error: $4 Not a valid format"
  exit 1
else
  MAX_VALUE=$4
  case "$MAX_VALUE" in
    ^[0-9]+$)
      MAX_VALUE=$2
    ;;
    x*)
      MAX_VALUE=$((16#${MAX_VALUE:1}))
    ;;
    b*)
      # MEM_WIDTH=$((16#${MEM_WIDTH:1}))
    ;;
  esac
  echo_i "max value : $MAX_VALUE"
fi



# get divisor
if ! [[ $5 =~ ^[xb]?[0-9]+$ ]] ; then
  echo_e "error: $5 Not a valid format"
  exit 1
else
  VALUE_DIV=$5
  case "$VALUE_DIV" in
    ^[0-9]+$)
      VALUE_DIV=$2
    ;;
    x*)
      VALUE_DIV=$((16#${VALUE_DIV:1}))
    ;;
    b*)
      # MEM_WIDTH=$((16#${MEM_WIDTH:1}))
    ;;
  esac
  echo_i "divisor : $VALUE_DIV"
fi

cnt=0
cnt_ctrl=$(($MEM_DEPTH/100))

MAX_VAL_OUT=$(printf '%x\n' "$(($MAX_VALUE))")
NUM_DIGITS=${#MAX_VAL_OUT}
echo_i "max MAX_VALUE : $MAX_VALUE MAX_VAL_OUT : $MAX_VAL_OUT NUM_DIGITS : $NUM_DIGITS"
exit
# echo $MEM_DEPTH
# echo $cnt_ctrl
echo "0%.......|.........|.........|.........|.........|.........|.........|.........|.........|......100%"
printf '%x\n' "$(($MAX_VALUE))" > $FULLFILEPATH
# for((i=1;i<$MEM_DEPTH;i++)); do
#   out_st=$(printf '%020x\n' "$(($MAX_VALUE/$i))")
  
#   echo "${out_st: -${NUM_DIGITS}}" >> $FULLFILEPATH
#   # printf '%020x\n' "$(($MAX_VALUE/$i))" >> $FULLFILEPATH

#   # printf '%0${NUM_DIGITS}x\n' "$(($MAX_VALUE/$i))" >> $FULLFILEPATH
  
#   if [ "$cnt" -gt "$cnt_ctrl" ]; then
#     printf "%s" "*"
#     cnt=0
#     # echo "Hola"
#   else
#     cnt=$(($cnt+1))
#     # echo "adios $cnt"
#   fi
 
# done
old_val=$(($MEM_DEPTH+1))
cnt_val=0
glob_cnt=0
for((i=1;i<$MEM_DEPTH;i++)); do
  new_val=$(($MAX_VALUE/$i))
  if [ $new_val == $old_val ]; then
    cnt_val=$(($cnt_val+1))
  else
    echo "$glob_cnt ::: $old_val --- $cnt_val"
    old_val=$new_val
    glob_cnt=$(($glob_cnt+1))
    cnt_val=0
  fi
  

done
printf "\n"
echo_i "Done!!!"

