--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: ATLAS L0MDT Trigger
-- File: VU_generic_pipelined_MATH_pkg.vhd
-- Module: <<moduleName>>
-- File PATH: /shared/arithmetics/pkg/VU_generic_pipelined_MATH_pkg.vhd
-- -----
-- File Created: Monday, 15th November 2021 9:29:29 pm
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Wednesday, 21st September 2022 1:43:51 am
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library shared_lib;
use shared_lib.vhdl2008_functions_pkg.all;

package VU_generic_pipelined_MATH_pkg is

  function arith_get_in_width(len , val : integer) return integer;
  
  function arith_get_out_width(op : string; gOW, AW, BW , CW, DW : integer) return integer;
  
end package VU_generic_pipelined_MATH_pkg;

package body VU_generic_pipelined_MATH_pkg is

  function arith_get_in_width(len,val : integer) return integer is
    variable ow : integer;
  begin
    if len = 1 then
      if val = 0 then
        ow := 0;
      else
        ow := 1;
      end if;
    else
      ow := len;
    end if;
    return ow;
  end function;
  
  function arith_get_out_width(op : string; gOW, AW, BW , CW, DW : integer) return integer is
    variable OW : integer;
  begin
    if gOW = 0 then
      if op="*" or op="*1" or op="*2" or op="*3" or op="*4" or op="*5" or op="*6" or op="*7" or op="*8" or op="*9" or op="*10" or op="*11" then 
        OW := AW + BW;
      elsif op = "+" then
        OW := fmax(AW,BW) + 1;
      elsif op = "-" OR  op = "--" then
        OW := fmax(AW,BW);
      elsif op = "/" then
        OW := fmax(AW,BW);
      elsif op = "*-" then
        OW := fmax(AW + BW,CW);
      elsif op="+++" then
        OW := 4 + fmax(AW, fmax(BW, fmax(CW,DW)));
      end if;
    else
      OW := gOW;
    end if;

    return OW;
  end function;
  
end package body VU_generic_pipelined_MATH_pkg;