--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: pkg
-- File: vhdl_time_pkg.vhd
-- Module: <<moduleName>>
-- File PATH: /vhdl_time_pkg.vhd
-- -----
-- File Created: Thursday, 15th August 2024 5:20:10 pm
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Thursday, 15th August 2024 5:20:19 pm
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

package vhdl_time_pkg is
  constant C_CURRENT_TIME : string := "00:00:00";
  constant C_CURRENT_DATE : string := "yyyy.mm.dd";
end package;