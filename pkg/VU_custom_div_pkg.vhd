--------------------------------------------------------------------------------
-- UMass , Physics Department
-- Project: pkg
-- File: VU_custom_div_pkg.vhd
-- Module: <<moduleName>>
-- File PATH: /VU_custom_div_pkg.vhd
-- -----
-- File Created: Tuesday, 6th December 2022 12:37:01 am
-- Author: Guillermo Loustau de Linares (guillermo.ldl@cern.ch)
-- -----
-- Last Modified: Tuesday, 6th December 2022 12:38:45 am
-- Modified By: Guillermo Loustau de Linares (guillermo.ldl@cern.ch>)
-- -----
-- HISTORY:
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- use ieee.math_real.all;
use ieee.std_logic_misc.all;


package VU_custom_div_pkg is
  
  -- function f_dig_get_mem_depth(SCALAR,SCALAR_10X : integer; QUOTIENT_LEN : std_logic_vector) return integer;

  -- type div_mem_t is array (0 to f_dig_get_mem_depth(SCALAR,SCALAR_10X,QUOTIENT_LEN) - 1) of std_logic_vector(QUOTIENT_LEN - 1 downto 0);

  -- function f_div_init_mem(SCALAR,SCALAR_10X : integer; QUOTIENT_LEN : std_logic_vector) return div_mem_t;

end package;

package body VU_custom_div_pkg is
  
  -- function f_dig_get_mem_depth(SCALAR,SCALAR_10X : integer; QUOTIENT_LEN : std_logic_vector) return integer is
  --   variable o_md : integer;
  -- begin
  --   o_md := 2**QUOTIENT_LEN;
  --   return o_md;
  -- end function;

  -- function f_div_init_mem(SCALAR,SCALAR_10X : integer; QUOTIENT_LEN : std_logic_vectorN) return div_mem_t is
  --   variable o_mem : div_mem_t;
  -- begin
  --   for i in 0 to 10 loop
      
  --   end loop;
  --   return o_mem;
  -- end function;
  
  
end package body VU_custom_div_pkg;